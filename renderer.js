// This file is required by the index.html file and will
// be executed in the renderer process for that window.
// No Node.js APIs are available in this process because
// `nodeIntegration` is turned off. Use `preload.js` to
// selectively enable features needed in the rendering
// process.
let fileInput = document.querySelector("#file-input");
let nameOfFile = document.querySelector(".name");
let size = document.querySelector(".size");
let lastModifiedDate = document.querySelector(".last-modified");
let type = document.querySelector(".type");

function upload(event) {
  let input = event.target;
  for (let i = 0; i < input.files.length; i++) {
    nameOfFile.textContent = input.files[i].name;
    size.textContent = input.files[i].size;
    lastModifiedDate.textContent = input.files[i].lastModifiedDate;
    type.textContent = input.files[i].type;
  }
}
fileInput.addEventListener("change", upload);
